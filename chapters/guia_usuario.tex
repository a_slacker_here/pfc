\chapter{Guía de uso}

Durante este capítulo se explicarán los pasos que se han de seguir
para diseñar un mecanismo leva-palpador utilizando QtCAM.

\section{Introducción de los parámetros de partida}
\label{sec:init-param}

Nada más ejecutar el programa, la pestaña de \emph{Cam design process} y la
subpestaña \emph{Starting data} serán seleccionadas por defecto
(Figura \ref{fig:ventana-inicial}), para
mostrar los \emph{widgets} de lectura de información y tal como se
comenta en el apartado \ref{subsec:design-view}, el emplazamiento de
los artefactos define un orden. Siguiendo este orden los datos que se
introducen son:

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.3]{images/vista-principal-nada.png}
  \caption{Ventana inicial}
  \label{fig:ventana-inicial}
\end{figure}

\begin{itemize}
\item \textbf{Tipo de mecanismo}: para definir el tipo de
  mecanismo, hacer click sobre el \emph{cuadro combinado}\footnote{Es
    más conocido por el término anglosajón \emph{combo box}} y
  seleccionar el mecanismo leva-palpador deseado (Figura
  \ref{fig:tipo-mecanismo}). QtCAM soporta la
  leva con seguidor de contacto unilateral, la leva conjugada con
  palpador de cierre por fuerza (contacto bilateral) y la leva
  desmodrómica de anchura constante (también contacto bilateral).
  \begin{figure}[h]
    \centering
    \includegraphics[scale=0.6]{images/select-cam-follow-mech.png}
    \caption{Selección del tipo de mecanismo}
    \label{fig:tipo-mecanismo}
  \end{figure}
\item \textbf{Datos de la leva}: A la caja de la izquierda se le añade
  un real que equivale al  valor de la velocidad angular a la que rota
  la leva en rad/s. A la de la derecha se le introduce un entero que
  corresponde al número de secciones en la que la leva estará dividida
  que se corresponde con el número de tramos que tendrá la ley del
  movimiento a diseñar.
  (figura \ref{fig:datos-leva}).

  \begin{figure}[h]
    \centering
    \includegraphics[scale=0.55]{images/cam-data-input.png}
    \caption{Datos de la leva}
    \label{fig:datos-leva}
  \end{figure}
\item \textbf{Tipo de palpador y su movimiento}: Hacer click sobre el
  cuadro combinado y seleccionar el tipo de palpador con el movimiento
  del mismo. Los tipos
  soportados por la aplicación son los mostrados en la ventana de la
  Figura \ref{fig:follower-movement-type}:

  \begin{figure}[h]
    \centering
    \includegraphics[scale=0.55]{images/follower-motion-type.png}
    \caption{Selector del tipo de seguidor con su movimiento}
    \label{fig:follower-movement-type}
  \end{figure}

\item \textbf{Parámetros geométricos}: Los datos a introducir cambian
  en función del tipo de mecanismo seleccionado. La notación usada en
  la aplicación concuerda con la mismo notación usada en el capítulo
  \ref{cha:gener-del-perf}; mirar este capítulo para ver a qué
  característica geométrica corresponde cada parámetro.

  \begin{figure}[h]
    \centering
    \includegraphics[scale=0.55]{images/geometric-parameters.png}
    \caption{Introducción de parámetros geométricos}
    \label{fig:geom-para-gui}
  \end{figure}

\end{itemize}

Tras haber introducido todos los datos, hacer \emph{click} sobre la
subpestaña \emph{Movement law design} para pasar a la fase de diseño de
las leyes del movimiento.

\section{Definición de la ley del movimiento}
\label{sec:mov-laws-GUI}

En el contenedor aparecerán tantas cajas como número de tramos
especificadas en el apartado de introducción de parámetros de partida
(figura \ref{fig:cajas-leyes-desplazamiento}).

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.4]{images/cajas-movimiento.png}
  \caption{Cajas de diseño de la ley de movimiento}
  \label{fig:cajas-leyes-desplazamiento}
\end{figure}

\subsection{Leyes tradicionales}
\label{subsec:tradicionales}
Para las leyes tradicionales, los parámetros a definir por orden de
izquierda a derecha son:
\begin{itemize}
\item \textbf{Ángulo inicial}: Corresponde al ángulo de la leva que
  marca el inicio del tramo de la ley.
\item \textbf{Ángulo final}: Corresponde al ángulo de la leva que
  marca el final del tramo de la ley.
\item \textbf{Tipo de ley}: Corresponde al tipo de ley a elegir. La
  caja cambiará en función de la ley escogida. Todas las leyes
  soportadas por QtCAM extán expuestas al detalle en el capítulo
  \ref{ch:mov_law}.
\item \textbf{Posición inicial}: Corresponde al punto desde donde
  empieza la posición del palpador.
\item \textbf{Desplazamiento}: Corresponde al desplazamiento del
  palpador en el tramo, dicho de
  otra forma: a la posición final menos la posición inicial.
\end{itemize}

\subsection{Ley doble armónica}
\label{subsec:double-harmonic-gui}


Cuando la ley es armónica doble, al tratarse de una ley simétrica, al
diseñar el tramo de descenso se diseña también el de descenso por lo
que la caja cambiará y el segundo tramo será automáticamente diseñado
a partir de los parámetros del primer tramo. Por ejemplo: si el ángulo
inicial el tramo de ascenso es $0$ y el final es $A$, entonces el
ángulo inicial del tramo de descenso es $A$ y el ángulo final es
$2(A-0)$; y si la posición inicial es $0$ y el desplazamiento es $B$
en el tramo de ascenso, la posición inicial en el tramo de descenso es
$0 + B$ y el desplazamiento es $-B$ (figura \ref{fig:double-harmonic-box}).

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.5]{images/double-harmonic-gui.png}
  \caption{Caja para una ley doble armónica}
  \label{fig:double-harmonic-box}
\end{figure}

\subsection{Ley de Bézier}
\label{subsec:bezier-gui}

Cuando la ley de Bézier es seleccionada, la caja cambia y los
parámetros a introducir son otros ya que la ley de Bézier difiere
bastante del resto de leyes tradicionales.

En QtCAM se han programado dos modos de funcionamiento con Bézier: El modo de
\emph{selección de máximo} y el modo \emph{selección de punto inicial
  y punto final}.

\subsubsection{Modo \emph{selección de máximo}}

Nada más seleccionar Bézier del cuadro combinado, el modo
\emph{selección de máximo} estará marcado por defecto (figura
\ref{fig:max-selector}).

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.55]{images/bezier-max-gui.png}
  \caption{Caja Bézier en el modo \emph{selección de máximo}}
  \label{fig:max-selector}
\end{figure}

El modo de selección de máximo corresponde a una ley de ascenso-descenso con
un máximo en el centro y un valor mínimo en los extremos

Los parámetros a introducir en este modo de funcionamiento son:

\begin{itemize}
\item \textbf{Ángulo inicial}: Igual que en leyes tradicionales.
\item \textbf{Ángulo final}: Igual que en leyes tradicionales.
\item \textbf{Tipo de ley}: Igual que en leyes tradicionales.
\item \textbf{Posición más baja}: Corresponde a la posición base a
  partir de la cual se forma el ascenso.
\item \textbf{Punto máximo}: Corresponde al punto máximo central de la
  ley.
\item \textbf{Selección de modo}: Es un cuadro combinado con los dos
  modos de funcionamiento.
\item \textbf{Continuidad}: Corresponde a la continuidad que guarda el
  tramo de ley con otros tramos contiguas con valor cero en los
  extremos. Las continuidades permitidas para este modo son superior o
  igual a 1.
\end{itemize}

\subsubsection{Modo \emph{selección de punto inicial y punto final}}

En el modo \emph{selección de punto inicial y punto final}, se
selecciona la posición del primer punto de la curva y la posición de
su último punto correspondiente a ese tramo.

La caja vuelve a cambiar para adaptarse a los requisitos necesarios
(figura \ref{fig:inicial-final-gui}), los parámetros a introducir en
la caja són:

\begin{itemize}
\item \textbf{Ángulo inicial}: Igual que en leyes tradicionales.
\item \textbf{Ángulo final}: Igual que en leyes tradicionales.
\item \textbf{Tipo de ley}: Igual que en leyes tradicionales.
\item \textbf{Posición en el inicio}: Corresponde a la posición del
  primer punto de la curva.
\item \textbf{Posición en el fin}: Corresponde a la posición que del
  último punto de la curva.
\item \textbf{Selección de modo}: Es un cuadro combinado con los dos
  modos de funcionamiento.
\item \textbf{Continuidad}: Corresponde a la continuidad que guarda la
  ley con otras leyes continuas en cero con valor cero en los
  extremos. Las continuidades permitidas para este modo son superior o
  igual a 2.
\end{itemize}

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.6]{images/first-last-mode.png}
  \caption{Caja Bézier en el modo \emph{selección de punto inicial y
      punto final}}
  \label{fig:inicial-final-gui}
\end{figure}

\section{Graficación de la ley del movimiento}
\label{sec:grafication}

Una vez se hayan definido las ley en todo el dominio $[0, 360]$, se
pulsa el botón \emph{Plot} (figura \ref{fig:plot-button}) con el fin
de ejecutar las funciones que toman los datos introducidos, calculan
las leyes y las devuelven graficadas en el lienzo (figura \ref{fig:graphs-gui}).

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.6]{images/plot-button.png}
  \caption{Bottón para ejecutar la función de generar puntos y mostrar
    gráficos}
  \label{fig:plot-button}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.3]{images/graphs.png}
  \caption{Ejemplo de una graficación}
  \label{fig:graphs-gui}
\end{figure}

\subsection{Visualización y manipulación de los gráficos}
\label{subsec:graph-manipulation}

Tras pulsar el botton \emph{Plot}, las gráficos serán mostrados
automáticamente en el lienzo tal como se muestra en la figura
\ref{fig:graphs-gui}. Justo debajo, hay una barra de herramientas con
un conjunto de utilidades para poder manipular los gráficos, a
continuación, se expondrá lo que hacen cada uno de los
iconos. Recalcar el hecho de que este panel estará debajo de cada
gráfico en el programa y su uso es idéntico en todos.

\begin{itemize}
\item \includegraphics[scale=0.7]{images/home.png} \textbf{Vista
    original}: Al pulsar sobre el icono, todos los gráficos del lienzo
  vuelven a su vista original, en caso de que los gráficos no hallan
  sido manipulados, no pasará nada.
\item \includegraphics[scale=0.7]{images/previous.png} \textbf{Vista
    previa}: Al pulsar sobre el icono, se vuelve a la vista anterior a
  la última manipulación. Si no había vista anterior no pasa nada.
\item \includegraphics[scale=0.7]{images/next.png} \textbf{Vista
    siguiente}: Al pulsar sobre el icono, se vuelve a la vista
  siguiente. Si no había vista siguiente no pasa nada.
\item \includegraphics[scale=0.7]{images/move.png} \textbf{Mover}: Al
  pulsar sobre el icono, se activa la herramienta para mover el
  gráfico: botón izquierdo del ratón para agarrar el gráfico y mover
  su contenido, botón derecho para agrandar o empequeñecer.
\item \includegraphics[scale=0.7]{images/zoom.png} \textbf{Zoom}: Al
  pulsar sobre el icono se activa la herramienta de para amplificar
  zonas; para usarla, basta con mantener pulsado el botón izquierdo
  del ratón y trazar un rectángulo en la zona que se quiera
  ampliar. En caso de que se quiera disminuir la ampliación, pulsar el
  botón derecho del ratón y trazar otro rectángulo.
\item \includegraphics[scale=0.7]{images/subgraf-conf.png}
  \textbf{Configuración de subgráficos}: Al pulsar sobre el icono, en
  pantalla saldrá el diálogo de configuración de los subgráficos. Cómo
  usar el diálogo se tratará más adelante.
\item \includegraphics[scale=0.7]{images/export-tool.png}
  \textbf{Exportar gráfico}: Al pulsar sobre el icono, se puede
  exportar la imagen en distintos formatos.
\item \includegraphics[scale=0.7]{images/figure-options.png}
  \textbf{Opciones de figura}: Al pulsar sobre el icono, aparece el
  diálogo de configuración de la figura. Cómo usar el diálogo se
  tratará más adelante.
\end{itemize}

\subsubsection{Diálogo de configuración de subgráficos}

Este diálogo permite configurar la posición relativa de los
subgráficos y consiste en una serie de interruptores correderos
(figura \ref{fig:subgraph-gui}).

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.5]{images/subplot-dialog.png}
  \caption{Diálogo de configuración de subgráficos}
  \label{fig:subgraph-gui}
\end{figure}

Los interruptores correderos situados en forma de cruz sirven para
cambiar el tamaño del conjunto de gráficos moviendo la parte a la que
le corresponde, por ejemplo: moviendo la corredera de la izquierda
cambiará el tamaño de los gráficos cediendo por la parte de la
izquierda y así con el resto de correderas.

Los interruptores correderos situados en la parte derecha sirven para
modificar el espaciado entre subgráficos: El de arriba cambia el
espaciado vertical entre gráficas mientras que el de abajo cambia el
espaciado horizontal.

\subsubsection{Diálogo de opciones de figura}

Cuando hay más de un gráfico en un mismo lienzo, aparecerá un diálogo
con un cuadro desplegable de donde seleccionar el gráfico a modificar
(figura \ref{fig:selec-graph}), una vez se halla escogido uno, pulsar
sobre el \emph{Ok} para que aparezca el diálogo de configuración de
ese gráfico (figura \ref{fig:opciones-figura}).

\begin{figure}[h]
  \centering
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\linewidth]{images/selec-graph-gui.png}
  \end{subfigure}
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\linewidth]{images/selec-graph-gui2.png}
  \end{subfigure}
  \caption{Diálogo de selección de gráfico}
  \label{fig:selec-graph}
\end{figure}

El diálogo de configuración aparecerá con la pestaña \emph{axes}
(ejes) selecciona por defecto (figura \ref{subfig:axes}). El primer
parámetro modificable es el título que aparecerá sobre el subgráfico,
y luego se dividirán los parámetros en dos partes: eje \emph{X} y eje
\emph{Y}. Las modificaciones son idénticas en ambos lugares cambiando
únicamente el eje.

Los parámetros modificables son:

\begin{itemize}
\item \textbf{Título}: Corresponde al título que aparecerá sobre el
  gráfico.
\item \textbf{Mínimo}: Corresponde la valor mínimo que aparecerá en el
  eje correspondiente (límite inferior del eje).
\item \textbf{Máximo}: Corresponde al valor máximo que aparecerá en el
  eje correspondiente (límite superior del eje).
\item \textbf{Etiqueta}: Corresponde al texto que aparecerá junto al
  eje correspondiente.
\item \textbf{Escala}: Es un cuadro desplegable con dos opciones:
  \emph{lineal} y \emph{logarítmico}; y corresponde a la escala a la
  que estará representada la curva respecto al eje correspondiente.
\end{itemize}

\begin{figure}[h]
  \centering
  \begin{subfigure}[b]{0.3\textwidth}
    \includegraphics[width=\linewidth]{images/figure-options-gui.png}
    \caption{Manipulación de ejes}
    \label{subfig:axes}
  \end{subfigure}
  \begin{subfigure}[b]{0.3\textwidth}
    \includegraphics[width=\linewidth]{images/figure-options-gui-curves.png}
    \caption{Manipulación de curvas}
    \label{subfig:curves}
  \end{subfigure}
  \caption{Diálogo de opciones de figura}
  \label{fig:opciones-figura}
\end{figure}

Se selecciona la otra pestaña para poder modificar los parámetros de
las curvas, los que marcan su representación (figura
\ref{subfig:curves}).

Los parámetros modificables comunes son:

\begin{itemize}
\item \textbf{Curva seleccionada}: El gráfico está compuesto por
  diversas curvas, es necesario seleccionar la curva a modificar.
\item \textbf{Etiqueta}: Para facilitar la distinción entre las
  curvas, se puede cambiar el nombre de la curva seleccionada.
\end{itemize}

Cabe recordar que las curvas están generadas por una larga serie de
puntos. Por defecto, el programa no muestra estos puntos pero sí que
muestra la línea que une uno con el siguiente. Las opciones de cómo
mostrar las líneas serán las nombradas a continuación:

\begin{itemize}
\item \textbf{Estilo de línea}: Corresponde al estilo con el que se
  quiere la línea. Alguno de los soportados son: sólido, escalón,
  punteada y \emph{ninguno}.
\item \textbf{Grosor}: Corresponde al grosor de línea.
\item \textbf{Color de línea}: Corresponde al color de la línea. Se
  puede especificar el color de dos formas distintas: mediante
  \emph{hex code} o utilizando el diálogo se selección de color.
\end{itemize}

Los parámetros modificables referente a cómo son mostrados los puntos
son:

\begin{itemize}
\item \textbf{Estilo}: Corresponde a la forma que tendrá el elemento
  de localización de puntos. Algunos de los soportados por el programa
  son: diamante, punto, \emph{X} y \emph{ninguno}
\item \textbf{Tamaño}: Corresponde al tamaño del elemento de
  localización de puntos.
\item \textbf{Color de cara}: Corresponde al color en el interior del
  elemento de localización de puntos. Se
  puede especificar el color de dos formas distintas: mediante
  \emph{hex code} o utilizando el diálogo se selección de color.
\item \textbf{Color de borde}: Corresponde al color del trazo del
  borde del elemento de localización de puntos. Se
  puede especificar el color de dos formas distintas: mediante
  \emph{hex code} o utilizando el diálogo se selección de color.
\end{itemize}

\subsubsection{Ampliar gráficos}

QtCAM hace posible ampliar y estudiar en una ventana dedicada,
cualquiera de los gráficos mostrados en la figura
\ref{fig:graphs-gui}, para ello, se pulsa sobre \emph{View} en la
barra de menú de la parte superior de la ventana para desplegar el
submenú (figura \ref{fig:submenu}):

\begin{figure}[h]
  \centering
  \begin{subfigure}[b]{0.5\textwidth}
    \includegraphics[width=\linewidth]{images/menu-view.png}
  \end{subfigure}
  \begin{subfigure}[b]{0.3\textwidth}
    \includegraphics[width=\linewidth]{images/menu-view2.png}
  \end{subfigure}
  \caption{Submenú de selección de gráfico}
  \label{fig:submenu}
\end{figure}

En el submenú se verán listados los gráficos a visualizar, al
seleccionar uno de ellos, aparecerá una ventana con un lienzo dedicado
al gráfico seleccionado (figura \ref{fig:desplazamiento-grafico}).

\begin{figure}[]
  \centering
  \includegraphics[scale=0.3]{images/desplazamiento-ampliado.png}
  \caption{Diálogo con el gráfico de la función desplazamiento}
  \label{fig:desplazamiento-grafico}
\end{figure}

Recordar el hecho de que el panel de herramientas para manipular el
gráfico estará siempre debajo de su correspondiente lienzo.

\section{Generación de perfil}
\label{sec:profile-gen}

Una vez el usuario esté de acuerdo con los parámetros y las leyes
introducidas, el siguiente paso es pulsar el botón
\emph{generate profile} situado en la parte inferior izquierda del
contenedor de pestañas 

\begin{figure}[]
  \centering
  \begin{subfigure}[b]{0.5\textwidth}
    \includegraphics[width=\linewidth]{images/generate-profile-all.png}
  \end{subfigure}
  \begin{subfigure}[b]{0.3\textwidth}
    \includegraphics[width=\linewidth]{images/generate-profile-but.png}
  \end{subfigure}
  \caption{Submenú de selección de gráfico}
  \label{fig:submenu}
\end{figure}

\section{Visualización de resultados}
\label{sec:results-gui}

Todos los resultados referentes a la generación del perfil están
contenidos en la pestaña \emph{Results} y, dentro de ella, se halla
otro contenedor pestañas que divide los resultados en dos categorías:
\emph{Cam profile} --que corresponde a la representación gráfica del
perfil junto con otros gráficos directamente relacionados-- y
\emph{Summary} --que corresponde a una recopilación de parámetros
importantes--.

\subsection{Pestaña \emph{Cam profile}}
\label{subsec:cam-profile-tab}

El contenedor está dividido en dos zonas: la zona de visualización
del perfil situado en la parte derecha de la pantalla y la zona donde
las curvas del radio de curvatura y el ángulo de presión están
graficadas en función del ángulo de leva rotado (figura \ref{fig:perfil-leva-gui}).

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.3]{images/cam-profile-gui.png}
  \caption{Vista del perfil de leva}
  \label{fig:perfil-leva-gui}
\end{figure}

El perfil principal de la leva está indicado en azul con un mayor
grosor de línea; el círculo con el trazo discontinuo representa el
círculo base y la línea roja representa el \emph{offset}
correspondiente al palpador de cabeza circular seleccionado.

En el caso de levas conjugadas, el perfil generado a partir del
perfil original se trazará en color verde al igual que su
correspondiente curva en el gráfico del radio de curvatura y ángulo de
presión.

\subsection{Pestaña \emph{Summary}}
\label{subsec:summary-tab}

Se presentará una vista con dos entornos de tablas, uno en la parte superior y
otro en la parte inferior (figura \ref{fig:summary-gui}).

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.3]{images/summary-gui.png}
  \caption{Pestaña \emph{Summary}}
  \label{fig:summary-gui}
\end{figure}

La tabla superior contiene los valores máximo y mínimo de los
parámetros calculados más importantes junto con las evaluaciones
necesarias para determinar si el perfil es o no adecuado. En al tabla
\ref{tab:sup} se resume su contenido.

Cuando la leva es conjugada, en la tabla aparecerán las filas
correspondientes al radio de curvatura y al ángulo de presión del
perfil conjugado.

Para que el perfil pueda ser considerado adecuado, las funciones
desplazamiento, velocidad y aceleración han de ser continuas; el
perfil ha de estar exento de picos y rebajes, y el radio de curvatura
ha de estar por debajo de $30º$ en caso de que el palpador tenga
movimiento de translación y por debajo de $35º$ cuando el palpador
tenga movimiento de rotación.

La tabla de la parte inferior contiene un resumen de los parámetros
introducidos junto con el sentido de giro de la leva y el tipo de
mecanismo. Resaltar el hecho de que cuando el mecanismo es con leva
desmodrómica de anchura
constante, la distancia entre los brazos del palpador serán
mostrados. Si el seguidor es oscilatorio, se mostrará el ángulo inicial.

\begin{table}[h]
  \centering
  \begin{tabular}[c]{|p{3cm}|p{3cm}|p{3cm}|p{3cm}|}
    \hline
    & \textbf{Máximo} & \textbf{Mínimo} & \textbf{Comentarios} \\
    \hline
    \textbf{Desplazamiento:} & Aquí se muestra el valor máximo de la función
                      desplazamiento & Aquí se muestra el valor mínimo
    de la función desplazamiento & Aquí se muestra si las funciones
                                   correspondientes a cada sección son
                                   continuas entre sí \\
    \hline
    \textbf{Velocidad:} & Aquí se muestra el valor máximo de la función
                      velocidad & Aquí se muestra el valor mínimo
    de la función velocidad & Aquí se muestra si las funciones
                                   correspondientes a cada sección son
                                   continuas entre sí \\
    \hline
    \textbf{Aceleración:} & Aquí se muestra el valor máximo de la función
                      aceleración & Aquí se muestra el valor mínimo
    de la función aceleración & Aquí se muestra si las funciones
                                   correspondientes a cada sección son
                                   continuas entre sí \\
    \hline
    \textbf{Sobreaceleración:} & Aquí se muestra el valor máximo de la función
                      sobreaceleración & Aquí se muestra el valor mínimo
    de la función sobreaceleración & Aquí se muestra si las funciones
                                   correspondientes a cada sección son
                                   continuas entre sí \\
    \hline
    \textbf{Radio de curvatura:} & Aquí se muestra el valor máximo de la función
                      del radio de curvatura & Aquí se muestra el valor mínimo
    de la función del radio de curvatura & Aquí se muestra los
                                           problemas que tiene el
                                           perfil \\
    \hline
    \textbf{Ángulo de presión:} & Aquí se muestra el valor máximo de la función
                      del ángulo de presión & Aquí se muestra el valor mínimo
    de la función del ángulo de presión & Aquí se muestra se el ángulo
    de presión es aceptable para la aplicación\\
    \hline
  \end{tabular}
  \caption{Esquema del contenido de la tabla superior}
  \label{tab:sup}
\end{table}

\section{Exportar perfil de leva}
\label{sec:export-cam}

Para exportar el perfil de leva en un archivo dxf, seleccionar
\emph{File} de la barra de menú y luego hacer \emph{click} sobre
\emph{exportar}. Aparecerá un diálogo en el que se ha de introducir el
nombre que uno le quiera otorgar al archivo seguido de la extensión
\emph{dxf}, por ejemplo: \emph{leva.dxf}.

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.6]{images/export-gui.png}
  \caption{Opción de exportar el perfil de la leva}
  \label{fig:export-profile}
\end{figure}
% \begin{itemize}
% \item \textbf{Desplazamiento}: Corresponde al \emph{array} con los
%   valores del desplazamiento. El primer valor corresponde al máximo,
%   el segundo al valor mínimo, y la tercera casilla muestra si el
%   conjunto de curvas del desplazamiento de cada sección son continuas
%   entre sí.
% \item \textbf{Velocidad}: Corresponde al \emph{array} con los valores
%   de la velocidad (primera derivada de la función desplazamiento). El
%   valor de la primera celda corresponde al máximo de la curva, el
%   segundo al mínimo y la tercera casilla comunica si el conjunto de
%   funciones de las diferentes secciones son continuas entre sí.
% \item \textbf{Aceleración}: Corresponde al \emph{array} con los
%   valores de la aceleración (segunda derivada de la función
%   desplazamiento). El valor de la primera celda corresponde al valor
%   máximo de la curva, el segundo corresponde al valor mínimo y la
%   tercera casilla informa de si cada una de las funciones que
%   corresponde a cada sección es continua con sus adyacentes.
% \item \textbf{Sobreaceleración}: Corresponde al \emph{array} con los
%   valores de la sobreaceleración (tercera derivada de la función
%   desplazamiento). El valor de la primera celda al componente de la
%  \emph{array} con el valor más alto, el de la segunda casilla
%  corresponde al componente de valor más bajo y la tercera informa de
%  si cada una de las funciones que corresponde a cada sección es
%  continua con sus adyacentes.
% \item \textbf{Radio de curvatura del perfil original}: Corresponde al
%   \emph{array} con los valores calculados del radio de curvatura. El
%   valor de la primera celda corresponde al valor máximo, el de la
%   segunda corresponde al componente de valor más bajo y la tercer
% \item \textbf{Radio de curvatura del perfil generado}: Esta fila sólo
%   estará visible si la leva es conjugada. Corresponde al radio de
%   curvatura del perfil
%   conjugado que se construye a partir del perfil original, la primera
%   celda muestra el componente del \emph{array} que posee el valor más
%   alto, la segunda el del valor más bajo y la tercera casilla informa
%   de los problemas que tiene ese perfil.
% \item \textbf{Ángulo de presión del perfil original}: Corresponde al
%   \emph{array} con 
% \end{itemize}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
