\chapter{Introducción a las levas}
\label{ch:int_cam}

Los mecanismos \textbf{leva-palpador}, objeto de estudio en este trabajo, se
utilizan frecuentemente en muchas clases de máquinas, por ejemplo: en
motores de combustión interna, telares, máquinas herramientas,
sistemas robóticos, etc. Estos mecanismos son sencillos y poco
costosos, tienen pocas piezas móviles y son compactos. Además no son
difíciles de diseñar leyes de movimientos del palpador que tengan casi
cualquier característica deseada y los perfiles de levas que
proporcionan el movimiento al palpador según la ley requerida son
mecanizados por máquinas de control numérico. Todo ello hace que se
utilicen ampliamente en la maquinaria actual.

% Sistema de cuerpos concebido para convertir los movimientos de, y las
% fuerzas sobre, uno o diversos cuerpos, en movimiento restringidos de,
% y fuerzas sobre otros cuerpos.


El sistema leva-palpador es un \textbf{mecanismo}, y como tal, su función es el
de convertir los movimientos de, y las fuerzas sobre uno o diversos
cuerpos en movimientos de, y fuerzas sobre otros cuerpos.

Un mecanismo \textbf{leva-seguidor} o \textbf{leva-palpador} consiste
en la interacción de 2 elementos principales: el \textbf{palpador}
(también referido como \textbf{seguidor}) y la \textbf{leva}.
Cada uno de estos elementos dispone de un único grado de libertad
debido a la acción de un elemento fijo y, este hecho,
desemboca en una relación de dependencia entre coordenadas
generalizadas conocida comúnmente como \textbf{ley del desplazamiento
  del palpador} ya que el movimiento de la leva acciona el del
seguidor.

El mecanismo leva-seguidor más comúnmente conocido y utilizado es el
de leva rotatoria (figura \ref{fig:firstCam}) y será el principal
objeto de estudio. Las ventajas de un mecanismo de este tipo respecto
a otros que permitan una flexibilidad en su ley del movimiento
semejante serían su robustez ante condiciones adversas como el caso
del accionamiento de las válvulas de un motor de combustión interna,
su relativo menor coste y su sencillez; un accionamiento electrónico,
por ejemplo, permite definir una ley de movimiento arbitraria, pero es
más caro, más complicado y no goza de la misma robustez.

\begin{figure}
  \centering
  \includegraphics[scale=0.25]{images/cam-animated.png}
  \caption{Leva \cite{wiktionary16:_tappet}}
  \label{fig:firstCam}
\end{figure}

\section{Terminología y nomenclatura}
\label{sec:term}

En la figura \ref{fig:terminology} se muestra una leva plana de rotación con palpador de cabeza circular y la terminología asociada que se usará.

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.4]{images/term-stand.png}
  \caption{Terminología estándard usada en los mecanismos
    leva-palpador \cite{tesis} }
  \label{fig:terminology}
\end{figure}

A continuación, se pasa a describir cada término y también los parámetros que serán utilizados para las expresiones de generación de perfil:
\begin{itemize}
\item \textbf{Radio del Rodillo} ($R_f$): Es el radio que tiene el rodillo de un palpador de cabeza circular. Es un parámetro que impone el diseñador.
\item \textbf{Radio Base} ($R_{b}$): Corresponde al radio de valor más pequeño que puede tener la leva y éste es elegido por el diseñador. Define el tamaño ``base''del elemento.
\item \textbf{Circunferencia Base}: Es la circunferencia más pequeña con el centro en el centro de rotación de la leva y su tamaño viene definido por el \emph{radio base}.
\item \textbf{Curva de Paso o \emph{offset}}: Es la trayectoria que describe el punto central del rodillo del palpador de cabeza circular. En caso de que el palpador sea plano o puntual ($R_f = 0$), la curva \emph{offset} coincide con el perfil de la leva.
\item \textbf{Punto de Trazo}: Es el punto del palpador que describe la trayectoria que constituye el perfil de la leva (caso de palpador puntual o plano) o la curva \emph{offset} (caso de palpador de rodillo).
\item \textbf{Radio Primario} ($R_p$): Es la distancia más
  pequeña que hay entre el centro de rotación
  de la leva y el \emph{punto de trazo}, su expresión es:
  \begin{equation}
    \label{eq:pr}
    R_p = R_b + R_r
  \end{equation}
\item \textbf{Circunferencia Primaria}: Es la circunferencia que tiene como centro el centro de rotación de la leva y posee $R_p$ como radio.
\item \textbf{Tangente Común}: Recta tangente a la curva \emph{offset} en el punto coincidente con el punto de trazo con el palpador.
\item \textbf{Normal Común}: Recta perpendicular a la tangente común.
\item \textbf{Ángulo de Presión} ($\phi$): Es el ángulo que hay entre el vector velocidad del palpador y la normal común al perfil de leva en el punto geométrico de contacto.
\item \textbf{Excentricidad} ($\varepsilon$):Es la distancia que hay entre el eje a lo largo del cual se traslada el palpador y el
centro de rotación de la leva. Su valor puede ser nulo y en tal caso recibiría el nombre de palpador axial o alineado. Sólo
está presente en palpadores con movimiento de traslación.
\item \textbf{Posición Inicial} o \textbf{desplazamiento inicial}
  ($d_0$): Corresponde a la distancia mínima entre el palpador y el
  centro de rotación de la leva.
\item \textbf{Posición} ($d(\varphi)$): Corresponde a la posición del
  palpador para un determinado valor del ángulo $\varphi$. Se calcula
  mediante la expresión \ref{eq:d}, siendo $s(\varphi)$ el valor
  obtenido de la ley del desplazamiento para ese valor de $\varphi$.
  \begin{equation}
    \label{eq:d}
    d(\varphi) = s(\varphi) + d_0
  \end{equation}
\end{itemize}

\section{Clasificación de los mecanismos leva-palpador}
\label{sec:label}

Los sistemas leva-seguidor se clasifican según diversos criterios la
formas de clasificación más comunes en la
literatura se expresarán a lo largo de las siguientes subsecciones.

\subsection{Tipo de movimiento del palpador}
\label{subsec:follower-movement}

El movimiento que puede tener el palpador son los siguientes:

\begin{itemize}
\item Movimiento de traslación.
\item Movimiento de rotación.
\end{itemize}

Dentro del movimiento que tiene el seguidor en sí, también existe una
clasificación con la dirección que tiene el desplazamiento con
respecto al eje de rotación de la leva:

\begin{itemize}
\item \textbf{Movimiento radial}: Cuando el movimiento del palpador
  sigue el eje de rotación de la leva.
\item \textbf{Movimiento axial}: Cuando el movimiento del palpador
  tiene dirección radial.
\end{itemize}

\subsection{Tipo de cierre del par superior}
\label{subsec:label}

Para que la transmisión de movimiento y fuerzas sea correcto, es
necesario que no se interrumpa el contacto entre la leva y el
palpador.

\begin{itemize}
  \item  El \textbf{cierre por fuerza} se realiza aplicando una fuerza
  externa con el fin de mantener el contacto
  entre la leva y el seguidor. Normalmente se
  utiliza un resorte y se considera positiva en el sentido que
  favorece el contacto (figura \ref{fig:example-force}).
\item El \textbf{cierre por forma} consiste en confinar la leva en un
  recinto que está en contacto por la leva en dos puntos de forma que
  no se requiera ninguna fuerza externa para mantener el contacto
  (figura \ref{fig:example-form}). A los mecanismos leva-palpador con
  este tipo de seguidores se les llama \textbf{mecanismos desmodrómicos}.
\end{itemize}

\begin{figure}[h]
  \centering
  \begin{subfigure}[b]{0.3\textwidth}
    \includegraphics[width=\textwidth]{images/ejemplo-cierre-fuerza.png}
    \caption{Cierre por fuerza}
    \label{fig:example-force}
  \end{subfigure}
  \begin{subfigure}[b]{0.3\textwidth}
    \includegraphics[width=\textwidth]{images/ejemplo-cierre-forma.png}
    \caption{Cierre por forma}
    \label{fig:example-form}
  \end{subfigure}
  \caption{Palpadores con movimiento de traslación y distintos tipos
    de cierre \cite{tesis}}
  \label{fig:ejemplos-cierres}
\end{figure}

\subsection{Tipo de palpador}
\label{subsec:follower-type}

Normalmente cuando se habla de tipo de seguidor, se hace referencia a
la forma del cabezal que está en contacto con la leva. El proyecto
sólo abarcará los seguidores de cabeza plana, los de cabeza circular y
los de contacto puntual\footnote{El contacto puntual sigue el mismo
  principio que un palpador de cabeza circular pero haciendo cero el
  radio del rodillo}; no obstante, se hará mención de los palpadores
más nombrados en la literatura:

\begin{itemize}
  \item Palpador de \textbf{cara plana}. Éstos poseen la ventaja de ser más
    baratos y más compactos que los seguidores de \textbf{rodillo} (figuras \ref{fig:tipos-de-palpadores}c,
    \ref{fig:tipos-de-palpadores}d, \ref{fig:tipos-de-palpadores}h).
  \item Palpador de \textbf{cabeza circular} o \textbf{rodillo}. Este tipo de
    seguidor, posee la ventaja de ser
    sometido a mucha menos fricción por ser rodante; se suelen usar en
    maquinaria de producción por su fácil reemplazo y disponibilidad
    (figuras \ref{fig:tipos-de-palpadores}a y \ref{fig:tipos-de-palpadores}g).
  \item Palpador \textbf{puntual}. Es considerado como un caso
    particular del palpador de cabeza circular ya que surge de hacer
    cero el radio del rodillo (figuras \ref{fig:tipos-de-palpadores}b,
    \ref{fig:tipos-de-palpadores}j).
  \item Palpador \textbf{curvo} o \textbf{de forma de hongo}. Normalmente se
    diseñan y fabrican sobre pedido para cada aplicación
    (figuras \ref{fig:tipos-de-palpadores}e,
    \ref{fig:tipos-de-palpadores}f, \ref{fig:tipos-de-palpadores}i).
\end{itemize}

% \begin{figure}[h]
%   \centering
%   \includegraphics[scale=1.6]{images/motor.jpg}
%   \caption{Levas planas de rotación con seguidor plano de traslación
%     con cierre por fuerza en un motor de combustión interna \cite{wikipedia16:_leva} }
%   \label{fig:example-force}
% \end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.45]{images/tipos-de-palpadores.png}
  \caption{Tipos de palpadores \cite{tesis}}
  \label{fig:tipos-de-palpadores}
\end{figure}

\subsection{Tipo de restricciones de movimiento}
\label{sec:restricciones}

Existen dos categorías generales de restricción del movimiento
definidas por Norton en \cite{norton}, la
\textbf{posición crítica extrema} y el \textbf{movimiento de trayectoria
crítica}:

\begin{itemize}
\item La \textbf{posición crítica extrema} es la más fácil de implementar:
  se definen las posiciones inicial y final del seguidor
  (que corresponden a las posiciones extremas) y no se especifica
  ninguna restricción en el movimiento para pasar de una posición a
  otra.
\item El \textbf{movimiento de trayectoria crítica} se caracteriza por
  tener restricciones en los extremos pero también se define el
  movimiento o alguna de sus derivadas durante el intervalo completo o parte de
  él.
\end{itemize}

\subsection{Tipo de programa de movimiento}
\label{subsec:tipo-programa}

En un ciclo de movimiento, el valor de la posición del palpador
puede padecer tres cambios: que su valor crezca (S), que
decrezca (B) o que se mantenga constante (D). En la literatura,
sobretodo en Norton \cite{norton}, a estos tres eventos se les llama
subida, bajada y detenimiento respectivamente.

De acuerdo con lo expuesto por Zayas \cite{tesis} y Norton \cite{norton}, se
establecen tres programas de movimiento:

\begin{itemize}
\item \textbf{Subida-Bajada} (S-B): Es el programa más simple,
  consiste en un tramo en el que el palpador se aleja del centro de
  rotación de la leva seguido de otro tramo en el que se acerca. No
  hay detenimientos.
\item \textbf{Subida-Bajada-Detenimiento} (S-B-D): Este programa
  consta de un detenimiento antes del tramo de subida y después del
  tramo de descenso. no ha detenimiento entre la subida y la bajada.
\item \textbf{Subida-Detenimiento-Bajada-Detenimiento} (S-D-B-D):
  Este programa consta de dos detenimientos, uno al final del tramo de
  ascenso y otro al final del tramo de descenso.
\end{itemize}



\section{Secuencia de diseño}
\label{sec:diseno}

El objetivo de una leva en un mecanismo leva-palpador consiste en
transmitir al palpador un movimiento $q_p(q_l)$ siendo $q_l$ la
coordenada independiente de la leva y $q_p$ la coordenada
dependiente del palpador.

La secuencia de diseño tal como lo especifica Cardona y Clos
\cite{tmm} es la siguiente:

\begin{enumerate}
\item Diseño de la ley del desplazamiento $q_p(q_l)$.
\item Obtención del perfil de la leva encargada de impulsar el
  palpador según las leyes del desplazamiento definidas.
\item Comprobación de que el perfil no presente características que
  obstaculice el contacto leva-palpador.
\end{enumerate}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
