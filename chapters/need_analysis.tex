\chapter{Análisis funcional}
Durante este capítulo, se analiza el proceso de diseño de QtCAM para cubrir las
necesidades que surgen durante el diseño del mecanismo leva-palpador
mediante su uso.

\section{Descripción general de la aplicación}
\label{sec:description}

La aplicación goza de una interfaz gráfica donde
introducir los datos necesarios para la síntesis del mecanismo.

Se parte del tipo de mecanismo que el diseñador crea adecuado
 para la aplicación y se introducen los datos de partida para el
 diseño del mecanismo en las casillas
correspondientes. Posteriormente, se pasa al diseño de las leyes de
desplazamiento las cuales serán graficadas por el programa para su
evaluación antes de generar el perfil. Una vez generado el perfil de
leva, la aplicación permite visualizarlo
junto con la representación del radio de curvatura y el ángulo de
presión, parámetros que informan de si el perfil es adecuado a o no
para el funcionamiento correcto del mecanismo. Finalmente, devuelve un
sumario con los datos de partida y los valores máximo y mínimo del
desplazamiento, velocidad, aceleración, sobreaceleración, radio de
curvatura y ángulo de presión junto con una conclusión junto a cada
parámetro que indica si el diseño es adecuado o no.

% Figura

\section{Casos de uso}
\label{sec:user-case}

Un \textbf{caso de uso}, es una sucesión de pasos o actividades
necesarias para lograr un objetivo. Para definir un caso de uso, se
parte de un punto de partida llamado \textbf{precondición} y se fija
un fin llamado \textbf{postcondición} y luego se define un camino de
instrucciones que marque la transición de la precondición a la
postcondición.

Para una descripción general, se usa una herramienta gráfica llamada
\textbf{Diagramas de casos de uso} que consiste en una esquematización
que resume la interacción entre un \textbf{actor} o conjunto de
actores con un \textbf{sistema}.

En las posteriores subsecciones se introducen los elementos que
conforman un diagrama de casos de uso y se presentan las
interacciones requeridas para lograr distintos objetivos.

\subsection{Identificación de los elementos en los casos de uso}
\label{sec:elementos}

\begin{enumerate}
\item \textbf{Actor}: Es el rol asumido por una persona, sistema o entidad que tiene como
  objetivo lograr una meta mediante la interacción con el sistema. En
  el caso del diseño del mecanismo mediante QtCAM, el actor es el
  diseñador.
\item \textbf{Sistema}: Es la unidad encargada de cumplir el objetivo
  del actor. El sistema es el programa QtCAM.
\item \textbf{Caso de uso}: Es el conjunto de interacciones necesarias
  para cumplir un objetivo. Los objetivos son varios y se tratarán en
  las siguientes subsecciones.
\end{enumerate}

\subsection{Definir la ley del movimiento}
\label{sec:ley-mov}

Para definir la ley del movimiento, se introduce el número de tramos
que se corresponde con los eventos de detención, subida o bajada que
tendrá la ley deseada, y que por tanto, heredará la leva. 
Se introduce también la velocidad angular y el tipo de
mecanismo con el tipo de seguidor; posteriormente, se especifica el
tipo de ley para cada tramo, se fijan los parámetros de cada ley y se
confirma la elección de datos para que las leyes del movimiento queden
definidas. Una vez definidas, las leyes quedan patente en gráficos
para su estudio y exportación. En la figura
\ref{fig:definir-ley-del-movimiento} se esquematiza la interacción
actor-sistema: El caso de uso de la creación de tramos tiene de
precondición que se halla definido la velocidad angular, el número
de tramos, el tipo de mecanismo y seguidor, todo eso queda englobado
en la dependencia.

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.34]{images/leyes_desplazamiento_UML.png}
  \caption{Diagrama de casos de uso de la definición de las ley del
    movimiento}
  \label{fig:definir-ley-del-movimiento}
\end{figure}

\subsection{Generar el perfil y exponer resultados}
\label{sec:profiel-generation}

La precondición es que se haya definido la ley del movimiento; el
actor sólo debería de pulsar un botón una vez considere que la ley
graficada es adecuadas para la aplicación, el programa devuelve el
perfil y los gráficos del radio de curvatura y el ángulo de presión junto
con un sumario en donde se muestra los parámetros más importantes
junto con la validación de la leva generada. En la figura
\ref{fig:UML-perfil} se muestra un diagrama.

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.30]{images/generar_perfil.png}
  \caption{Diagrama de casos de uso de la generación del perfil}
  \label{fig:UML-perfil}
\end{figure}

\section{Descripción detallada de la aplicación}
\label{sec:label}

El objetivo de esta sección es el de exponer el conjunto de funciones
imprescindibles que el diseñador pueda necesitar.

\subsection{Descripción general}
\label{subsec:general-description}

La aplicación dispondrá de una sola ventana para introducir todos los
datos y sólo se usarán ventanas emergentes cuando se necesita ampliar
un visualizador. La ventana principal dispondrá de diferentes pestañas
organizadas en dos categorías: definición de los parámetros para el
diseño del mecanismo y exposición de resultados.

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.5]{images/esquema_ventana.png}
  \caption{Esquema de la ventana principal}
  \label{fig:ventana-esquema}
\end{figure}

Las unidades de longitud se expresan en milímetros en todos los casos,
las medidas angulares se expresan en grados, el
valor de la velocidad angular de la leva que se introduce en radianes
por segundo. Destacar el hecho de que se pueden introducir cuantos
decimales se quiera pero el sistema trabajará con 15 decimales
mientras que los resultados se presentarán con 3 decimales (hasta la
micra).

\subsection{Definición de las características del mecanismo}
\label{subsec:mecha-definition}

Para definir las características del mecanismo leva-seguidor, el
programa ha de facilitar la introducción de los parámetros necesarios.

Para que las características del mecanismo queden definidas se ha de
proporcinar la siguiente información:

\begin{enumerate}[label=\textbf{\alph*)}]
\item \textbf{Tipo de mecanismo leva-palpador}

Consiste en definir el tipo de mecanismo leva-palpador de acuerdo si
el contacto es unilateral (mecanismo con cierre por fuerza del par
superior) o si el contacto es bilateral (mecanismo desmodrómico).

La aplicación facilitará una lista de donde seleccionar el tipo de
mecanismo. Los soportados por el programa son:
\begin{itemize}
\item Mecanismo leva-palpador con contacto unilateral.
\item Mecanismo leva-palpador desmodrómico con leva conjugada.
\item Mecanismo leva-palpador desmodrómico de anchura constante.
\end{itemize}

\item \textbf{Datos de la leva}

Estos parámetros son la velocidad angular a la que
gira la leva y el número de secciones que tendrá la leva que
corresponde con el número de tamos que se quiere que tenga la ley de
desplazamiento.

\item \textbf{Tipo de palpador con su movimiento}

Consiste en escoger el tipo de seguidor junto con el tipo
de movimiento del mismo.

La aplicación ha de disponer una lista desde la cual el usuario podrá
seleccionar el tipo que se considere adecuado. La aplicación ha de ser
capaz de soportar los siguientes palpadores:

\begin{itemize}
\item Palpador de cabeza plana con movimiento de traslación.
\item Palpador de rodillo con movimiento de traslación.
\item Palpador puntual con movimiento de traslación
  (Palpador de rodillo siendo cero el radio de rodillo $R_r$).
\item Palpador de cabeza plana con movimiento de rotación.
\item Palpador de rodillo con movimiento de rotación.
\item Palpador puntual con movimiento de rotación (Palpador de rodillo
  siendo cero el radio de rodillo $R_r$).
\end{itemize}

Resaltar el hecho que para la leva desmodrómica de anchura constante,
solamente se implementará el seguidor plano de traslación, el resto de
palpadores están fuera del alcance del proyecto (se decidió no
incluirlos).

\item \textbf{Parámetros geométricos}

Consisten en el conjunto de parámetros que definen la geometría del
mecanismo de forma directa.

Para que el mecanismo quede totalmente definido, es necesario que los
siguientes parámetros se introduzcan:

\begin{itemize}
\item \textbf{Radio base ($Rb$)}: El radio base de la leva, el valor se
  expresa en milímetros (mm).
\item \textbf{Excentricidad ($\varepsilon$)}: Este valor es sólo
  relevante en seguidores de traslación, se trata de la separación que
  hay entre el eje que define la dirección del movimiento del palpador
  y el centro de rotación de la leva. Su valor se expresa en
  milímetros (mm).
\item \textbf{Inclinación del seguidor ($\beta$)}: El ángulo de
  inclinación que tiene las superficie plana de la cabeza del seguidor
  con la horizontal. Este parámetro solo tiene sentido en palpadores
  planos de traslación. Su valor se expresa en grados (º).
\item \textbf{Radio de la cabeza del palpador ($Rr$)}: Este parámetro
  sólo tiene sentido en seguidores con cabeza circular y define el
  radio del rodillo. Su valor se expresa en milímetros (mm).
\item \textbf{Distancia entre centros de rotación ($l_1$)}: Este
  parámetro sólo tiene sentido en seguidores con movimiento de
  rotación. Es la distancia que hay entre el centro de rotación de la
  leva y el centro de rotación del palpador.
\item \textbf{Longitud del brazo ($l_2$)}: Es la
  distancia que hay entre le centro de rotación de todo el palpador y
  la cara de contacto del seguidor con la leva. Se expresa en
  milímetros (mm) y se usa solamente es usado en el cálculo de levas con
  seguidor de cara plana y movimiento de rotación.
\item \textbf{Distancia hasta el centro del rodillo ($l_3$)}: Es la
  distancia que hay entre el centro del rodillo y el centro de
  rotación. Sólo se usa en el cálculo de perfiles con el seguidor de
  cara circular y movimiento de rotación. Se expresa en milímetros
  (mm).
\item \textbf{Distancia entre brazos ($d_c$)}: Corresponde a la distancia que
  hay entre los dos palpadores paralelos de un seguidor de contacto bilateral. Sólo
  es usado en las levas desmodrómicas con seguidor de movimiento de
  traslación. Se expresa en milímetros (mm).
\item \textbf{Ángulo de apertura entre brazos ($\beta$)}: Corresponde
  a la apertura que hay entre los dos brazos del seguidor. Sólo se usa
  en el cálculo de perfiles de levas desmodrómicas cuyo seguidor tenga
  movimiento angular. Se expresa en grados (º).
\end{itemize}

\end{enumerate}

\subsection{Definición de la ley del movimiento}

Una vez definido las características del mecanismo, la aplicación
ha de facilitar el diseño de las leyes del movimiento.

El número de secciones del perfil de leva introducido definen los
tramos de la ley de movimiento a
diseñar; el programa ha de proporcionar un asistente gráfico de diseño
para cada tramo y cada asistente ha de soportar la inclusión de los
siguientes datos:

\begin{itemize}
\item \textbf{Ángulo inicial}: Es el ángulo que marca el inicio del
  tramo de la ley de movimiento. Se expresa en grados (º).
\item \textbf{Ángulo final}: Es el ángulo que marca el fin del tramo de la
  ley de movimiento. Se expresa en grados (º).
\item \textbf{Posición inicial}: Es la posición que tiene el seguidor
  en el ángulo inicial ($\varphi = 0$). Se expresa en milímetros (mm) cuando el movimiento
  del palpador es de traslación y en grados (º) cuando el movimiento
  es de traslación.
\item \textbf{Desplazamiento}: Corresponde a los milímetros (mm)
  desplazados por el palpador cuando es translación o a los grados (º)
  girados cuando es de rotación.
\end{itemize}

\subsubsection{Ley de Bézier}

La aplicación ha de ser capaz de definir un tramo de ley de movimiento
mediante una curva de Bézier. El
comportamiento de esta ley es diferente al del resto por lo que los
únicos datos a introducir comunes al de las leyes tradicionales son
el ángulo inicial y el ángulo final.

Los datos a introducir en una ley de Bézier serían:

\begin{itemize}
\item \textbf{Modo}: A la hora de diseñar un desplazamiento con
  Bézier, se suelen emplear dos formas prácticas: una única ley que implique
  que un ascenso y un descenso en un solo tramo simétrico --por lo que
  se selecciona un valor máximo y un valor mínimo-- o un tramo de
  ascenso o descenso similar a la de las leyes tradicionales.

\item \textbf{Continuidad}: Este valor es un entero que marca la
  continuidad que posee el tramo de ley en los puntos de unión. La
  continuidad influye directamente en el número
  de ordenadas de Bézier empleados para definir la curva. Continuidad
  $0$ significa continuidad en el desplazamiento, continuidad $1$
  significa que la función y si primera derivada son continuas y así
  sucesivamente. Indica la suavidad en la transición entre tramos
  continuos.

\item \textbf{Posición en el inicio}: Corresponde a la posición que
  tendrá la ordenada de Bézier en el inicio del tramo. Se expresa en
  milímetros (mm) si el palpador es de traslación y en grados (º) si
  es de rotación. Sólo se usa en el modo \emph{primer y último}.

\item \textbf{Posición en el final}: Corresponde a la posición que
  tendrá la ordenada de Bézier en el final del tramo. Se expresa en
  milímetros (mm) si el palpador es de traslación y en grados (º) si
  es de rotación. Sólo se usa en el modo \emph{primer y último}.

\item \textbf{Posición base}: Corresponde a al valor de la ordenada de
  Bézier en el inicio y el final del tramo. Se expresa en milímetros (mm)
  si el seguidor es de traslación y en grados si es de rotación
  (º). Sólo se usa si el modo es \emph{selección del máximo}.

\item \textbf{Máximo}: Corresponde al valor máximo de la curva de
  Bézier en el punto medio del tramo y se expresa en milímetros (mm)
  si el seguidor es de traslación y
  en grados si es de rotación (º). Sólo tiene uso en el modo
  \emph{selección de máximo}.
\end{itemize}

\subsection{Visualización de las leyes del desplazamiento}
\label{subsec:laws-vis}

El programa ha de ser capaz de mostrar gráficamente las leyes del
desplazamiento, se procurará que los gráficos sean accesibles en
pantalla de diseño y no de visualización ya que es una importante
herramienta en la toma de decisiones.

\subsection{Visualización de resultados}
\label{subsec:results-visual}

La aplicación mostrará el perfil de la leva junto con las curvas del
radio de curvatura y el ángulo de presión. También ha de indicar si
los parámetros de comprobación muestran un perfil adecuado o no.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
