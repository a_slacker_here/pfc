\chapter{Verificación del perfil}
\label{chap:verification}

Una vez sintetizado el perfil, es necesario comprobar que
éste sea adecuado para el tipo de aplicación en que se quiere
utilizar, para ello se emplearán
dos herramientas: el \textbf{ángulo de pressión} y el \textbf{radio de
  curvatura} que permiten la evaluación del perfil y determinar si el
mecanismo del cual forma parte tendrá un correcto funcionamiento.

\section{Radio de curvatura}

El \textbf{radio de curvatura} se define como el radio que tiene el circulo
imaginario que un determinado punto de una curva tendría que trazar para conseguir
desplazarse al siguiente punto adyacente. No importa lo complicada que
sea la geometría de la curva ya que
cada uno de los puntos que la componen tendrá un centro geométrico y una
distancia al mismo.

El contorno o perfile de una leva es una curva cerrada sobre sí misma por lo que es posible utilizar
el concepto del radio de curvatura para su análisis geométrico.

El radio de curvatura puede ser usado para  comprobar los perfiles de
leva que impulsan todos los palpadores
anteriormente estudiados y permite detectar los dos tipos básicos de
problemas que pueden surgir en el perfil geométrico de una leva, estos
son:

\begin{enumerate}
\item Que el palpador no sea capaz de palpar puntos teóricos del perfil porque la geometría de la leva obstaculiza el contacto.
\item Que existan degeneraciones en el perfil como contactos puntuales --comúnmente conocidos como \emph{picos}-- o \textbf{autointersecciones}.
\end{enumerate}

La siguiente ecuación corresponde a la expresión genérica para deducir las
fórmulas del radio de curvatura para cualquier tipo de palpador a
partir de expresiones paramétricas (familia de puntos obtenidos
de la forma $(x(\varphi), y(\varphi))$):

\begin{equation}
  \label{eq:1n}
  r_{c} = \frac{(x'^2 + y'^2)^{\frac{3}{2}}}{(x' y'' - y' x'')}
\end{equation}

% No obstante, como para facilitar el cálculo se ha decidido usar una
% base giratoria, se puede usar la expresión \ref{eq:2n} en la base
% ${1,2}$ derivando según \ref{eq:3n}\footnote{A este procedimiento se le denomina ``derivación
% en base móvil''}:

Partiendo de la nomenclatura utilizada en el capítulo anterior en
donde se utiliza una base móvil $1,2$ y una base fija $x,y$ para el
cálculo del perfil de leva, se decide ser coherente con la
nomenclatura y expresar la determinación del radio de siguiendo la
misma metodología que Cardona y Clos \cite{tmm}.

El radio de curvatura se calcula mediante la siguiente expresión:

\begin{equation}
  \label{eq:2n}
  r_c = \frac{\abs{\overline{\mathbf{OP}}'}^2}{\left. \overline{\mathbf{OP}}'' \right|_{n}}
\end{equation}

siendo $\overline{\mathbf{OP}}$ la primera derivada respecto del
ángulo de giro $\varphi$ del vector de posición del
punto de contacto leva-palpador y siendo $\left. \overline{\mathbf{OP}}'' \right|_{n}$
el componente normal de la segunda derivada respecto del ángulo de
giro $\varphi$. El subíndice $n$ indica el componente normal
$\overline{\mathbf{OP}}''$ hacia el interior de la curva cerrada.

Como el cálculo se realiza desde la base móvil $1,2$, la derivación
respecto del ángulo de giro $\varphi$ se realiza mediante la siguiente
expresión:

\begin{equation}
  \label{eq:3n}
  \left\{ \overline{\mathbf{OP}}' \right\}_{1,2} = 
  \begin{bmatrix}
    0 & 1 \\
    -1 & 0
  \end{bmatrix}
\left\{ \overline{\mathbf{OP}} \right\}_{1,2} + \left\{ \overline{\mathbf{OP}} \right\}_{1,2}'
\end{equation}

El componente normal de la segunda derivada del vector de posición del
punto de contacto $\overline{\mathbf{OP}}$ se calcula con la siguiente
expresión:

\begin{equation}
  \label{eq:4n}
  \left. \overline{\mathbf{OP}}'' \right|_{n} = \frac{\mathbf{\overline{OP}''} \times \mathbf{\overline{OP}'}}{\abs{\overline{\mathbf{OP}}'}}
\end{equation}

\subsection{Análisis del radio de curvatura para un mecanismo
  leva-palpador con palpador plano}

Este análisis engloba a todos los palpadores de cara plana tratados en
el capítulo \ref{cha:gener-del-perf}.

Analizando el valor del radio de curvatura, se deduce los siguiente \cite{tmm} :

\begin{itemize}
\item Caso $r_{c}(\varphi)>0$: Significa que la leva es convexa y que el palpador es capaz
  de palpar ese punto sin problemas. Perfil \textbf{apto} para la
  aplicación con palpador plano.

\item Caso $r_{c}(\varphi) = 0$: Significa que el cambio de dirección que tendría que efectuar
  el punto para trasladarse a la posición de su adyacente inmediato es tan brusco que el
  centro geométrico se encuentra sobre el mismo punto; lo que significa que se genera una
  singularidad conocida como \emph{pico} o \emph{vértice} del perfil. Esta singularidad conlleva
  una insignificante superficie de contacto que a su vez provoca grandes
  presiones que pueden dañar el palpador. Perfil \textbf{no apto} para
  la aplicación con palpador plano.

\item Caso $r_{c}(\varphi) < 0$: Significa que el centro geométrico se encuentra fuera
  del confinamiento del perfil. Los puntos con radio de curvatura negativo se encuentran
  entre dos singularidades ($r_{c} = 0$) y el conjunto forma una subentidad geométrica
  llamada \emph{rebaje} (figura \ref{fig:1}). Perfil \textbf{no apto}
  para la aplicación con palpador plano.

\end{itemize}

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.45]{images/curvatureRadiusProb.png}
  \caption{Perfiles de leva no válidos: vértice en la imagen de la izquierda y un rebaje
    en la de la derecha \cite{tesis} }
  \label{fig:1}
\end{figure}

Se destaca el hecho de que el valor del radio de curvatura $r_c$ aumenta al hacerse más
grande el radio base $R_b$.

\subsection{Análisis del radio de curvatura para un mecanismo con palpador de rodillo}

Los palpadores de cabeza circular requieren dos estudios: uno para la curva \emph{offset}
y otro para el perfil geométrico de la leva. La expresión de cálculo para el radio de
curvatura del offset es la misma que la \ref{eq:2n} salvo que se usa
el vector $\mathbf{\overline{OC}}$ de posición del centro del rodillo $\mathbf{C}$
 en vez del vector $\mathbf{\overline{OP}}$ de posición del punto de
contacto $\mathbf{P}$.

El radio de curvatura $r_c$ del perfil se obtiene mediante la expresión
siguiente expresión:

\begin{equation}
  \label{eq:rcc}
  r_{c}(\varphi) = r_{cp}(\varphi) - R_r
\end{equation}

Siendo $r_{cp}$ el radio de curvatura de la curva \emph{offset}.

El siguiente análisis engloba a todos los mecanismos leva-palpador con
palpador de rodillo tratados en este escrito.

Analizando el radio de curvatura $r_c$, se deduce lo siguiente:
\begin{itemize}
\item Caso $r_{c} < 0$: Significa que la leva tiene un tramo cóncavo; la validez
  de este perfil para su aplicación depende de si el la cabeza circular del palpador puede
  palpar cada punto del tramo de radio de curvatura negativo:
  \begin{itemize}
  \item Caso $\abs{r_{c}} < R_r$: Significa que el rodillo del
    palpador es demasiado grande como para caber en la zona cóncava por lo que no hay
    contacto palpador-leva. Perfil \textbf{no apto} para la aplicación
    con palpador de rodillo.
  \item Caso $\abs{r_{c}} \geq R_r$: Significa que el contacto palpador-leva es
    correcto. Perfil \textbf{apto} para la aplicación con palpador de rodillo.
  \end{itemize}
\item Caso $r_{c} > 0$: Significa que la curva es convexa y que el
  análisis se traslada a
  al perfil \emph{offset}:
  \begin{itemize}
  \item Caso $r_{cp}= R_r$: Significa que se generará un vértice o \emph{pico} que
    traería los mismos problemas que en el caso de la leva con palpador plano. Perfil
    \textbf{no apto} para la aplicación con palpador de rodillo.
  \item Caso $r_{cp} < R_r$: Significa que se genera un rebaje. Perfil \textbf{no apto}
    para la aplicación con palpador de rodillo.
  \end{itemize}
\end{itemize}

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.35]{images/curvatureRadiusProbCirc.png}
  \caption{Ilustración de un pico (izquierda) y un rebaje (derecha) \cite{tesis} }
  \label{fig:curvatureRadiusProbCirc}
\end{figure}

\section{Ángulo de presión}

El \textbf{Ángulo de presión} se define como el ángulo existente entre la dirección del
desplazamiento del centro del rodillo (caso de palpador de cabeza circular) o del punto de
contacto del palpador con la leva(caso de palpador plano o puntual) y
la normal común al perfil de leva en el punto geométrico de contacto.

El cálculo solamente tiene sentido en palpadores de cabeza circular o puntual ya que
cuando el palpador es plano, entonces el ángulo de presión es coincide
con ángulo $\beta$ de inclinación del palpador.

Norton \cite{norton} plantea como regla empírica que es deseable que el ángulo de
presión se halle comprendido entre $0^{o}$ y
$30^o$ para el caso de un palpador de traslación y entre $0^o$ y
$35^{o}$ para un palpador de movimiento circular.

\section{Cálculo de radio de curvatura y ángulo de presión del
  mecanismo leva-palpador para cada tipo de palpador estudiado}
\label{sec:1}

Sabiendo las formas genéricas del cálculo del radio de curvatura $r_c$, es posible desarrollarlas para cada palpador
obteniendo, de esa forma, una expresión matemática sencilla e idónea para la programación o
para la resolución de problemas simples.

En las posteriores subsecciones, se exponen las expresiones de cálculo del radio
de curvatura $r_c$ y del ángulo de presión $\varPhi$ utilizadas en la
programación de verificación de perfil de leva para cada tipo de palpador.

\subsection{Caso de palpador de cara plana con movimiento de translación}
\label{subsec:2n}

Como se ha comentado anteriormente, no hay expresión de cálculo para
el ángulo presión $\varPhi$ ya que coincide con la inclinación $\beta$ del palpador.

% Se (\ref{eq:nozerobeta}):

% \begin{equation*}
% \left\{\mathbf{\overline{OP}} (\varphi) \right\}_{1, 2} =
%   \begin{Bmatrix}
%     d'(\varphi) \cos \beta \\
% d(\varphi) \cos \beta - \varepsilon \sin \beta
%   \end{Bmatrix}_{1, 2}
% \tag{\ref{eq:nozerobeta}}
% \end{equation*}

A partir de la ecuación \ref{eq:nozerobeta} y operando en la
\ref{eq:2n} siguiendo la metodología de Zayas \cite{tesis} , se
obtiene la ecuación del radio de curvatura, ésta es:

\begin{equation}
  \label{eq:vertp}
  r_{c} = (d(\varphi) + d''(\varphi)) \cos \beta - \varepsilon \sin \beta
\end{equation}

\subsection{Caso de palpador de rodillo con movimiento de traslación}
\label{subsec:3n}

En el caso de un mecanismo leva-palpador con un palpador de rodillo, hace falta comprobar tanto
el radio de curvatura $r_c$ como el ángulo de presión $\varPhi$.

\subsubsection{Radio de curvatura}
% Se recuerda la expresión de la curva \emph{offset} (\ref{eq:OC12}):

% \begin{equation}
%   \left\{ \mathbf{\overline{OC}} \right\}_{1,2} =
%   \begin{Bmatrix}
%     \varepsilon \\
% d(\varphi)
%   \end{Bmatrix}
% \tag{\ref{eq:OC12}}
% \end{equation}

Partiendo de la expresión \ref{eq:OC12} y operando con ella en la
expresión \ref{eq:2n} siguiendo lo expuesto por Zayas \cite{tesis}, se obtiene:

\begin{equation}
  \label{eq:6n}
  r_{cp} = \frac{(d^{2}(\varphi) + (d'(\varphi) - \varepsilon)^{2})^{\frac{3}{2}}}
  {(d'(\varphi) -
    \varepsilon) (2d'(\varphi) - \varepsilon) - d(\varphi)(d''(\varphi) - d(\varphi))}
\end{equation}

Esta ecuación permite obtener el radio de curvatura de la curva \emph{offset};
para conseguir la expresión del radio de curvatura $r_c$ del perfil de la
leva, se aplica la expresión \ref{eq:rcc}.

\subsubsection{Ángulo de presión}

Se usa la expresión \ref{eq:8n} propuesta por Norton \cite{norton}
para obtener el ángulo de presión $\varPhi$, esta es:

\begin{equation}
  \label{eq:8n}
  \varPhi = \arctan \frac{d'(\varphi) - \varepsilon}{d(\varphi) +
    \sqrt[]{R_{p}^2 - \varepsilon^2}}
\end{equation}

\section{Caso de palpador de cara plana y movimiento de rotación}
\label{sec:3n}

Para verificar el perfil de leva en este caso sólo se calcula el radio de curvatura
mediante la expresión \ref{eq:9n} expuesta en \cite{svelocity}  que
surge de usar la expresión \ref{eq:pOJ} en la
\ref{eq:2n}:

% \begin{equation}
%   \left\{ \mathbf{\overline{OP}} (\varphi) \right\}_{1,2} =
% \left\{
%   \begin{array}{r}
%     l_1 + l_2 \sin (d(\varphi)) - l_3 \cos (d(\varphi)) \\
% l_2 \cos (d(\varphi)) + l_3 \sin (d(\varphi))
%   \end{array}
% \right\}_{1,2}
% \tag{eq:pOP}
% \end{equation}


\begin{equation}
  \label{eq:9n}
  r_{c} = l_2 + l_1 \left( \frac{1 + 2 d'(\varphi)}{(1 +
      d'(\varphi))^{2}} \sin(d(\varphi))
    + \frac{d''(\varphi)}{(1 + d'(\varphi))^{3}} \cos(d(\varphi))
  \right)
\end{equation}

\section{Caso de palpador de rodillo con movimiento de rotación}
\label{sec:4n}

\subsection{Radio de curvatura}

% Se recuerda la expresión \ref{eq:aOC} y el hecho que el estudio se empieza por la curva offset:

% \begin{equation}
%   \left\{ \mathbf{\overline{OC}} (\varphi) \right\}_{1,2} = \left\{
%     \begin{array}{r}
%       l_1 - l_3 \cos (d(\varphi)) \\
% l_3 \sin (d(\varphi))
%     \end{array}
%   \right\}_{1,2}
% \tag{\ref{eq:aOC}}
% \end{equation}

A partir de la expresión \ref{eq:aOC} y operando en la \ref{eq:2n}, se
obtiene la expresión del radio de curvatura de la curva \emph{offset} $r_{cp}$,
esta es:

\begin{equation}
  \label{eq:rcco}
  r_{cp} = \frac{(A^2 + B^2)^{\frac{3}{2}}}{- l_1 l_3 d''(\varphi)
    \sin(d(\varphi)) + (B l_3
    d'(\varphi) \cos(d(\varphi)) + A^2)(d'(\varphi) + 1) + B^2}
\end{equation}

Siendo:

\begin{equation}
A = (d'(\varphi) + 1) l_3 \sin(d(\varphi))
\end{equation}

\begin{equation}
B = - l_1 + (d'(\varphi) + 1) l_3 \cos(d(\varphi))
\end{equation}

Para hallar el radio de curvatura del perfil de leva, aplicar la expresión \ref{eq:rcc}.

\subsection{Ángulo de presión}

El ángulo de presión $\varPhi$ se calcula mediante la expresión
expuesta en el trabajo de Serrano \cite{serrano}:

\begin{equation}
  \label{eq:pao0}
  \varPhi = \Psi + \alpha - \frac{\pi}{2}
\end{equation}

Siendo:

\begin{equation}
  \label{eq:pao}
  \Psi = \arccos \left( \frac{\abs{\mathbf{\overline{OC}}}^2 + l_{3}^2 - l_{1}^{2}} {
        2 \abs{\mathbf{\overline{OC}}} l_{3}} \right)
\end{equation}

\begin{equation}
  \label{eq:pao2}
  \alpha = \arctan \left( \frac{l_{3} \sin(\Psi) d'(\varphi)}{\abs{\mathbf{\overline{OC}}} - l_{3} \cos(\Psi) d'(\varphi) } \right)
\end{equation}

%%% Local Variables:
%%% mode: latex
%%% ispell-local-dictionary: "spanish"
%%% TeX-master: "../main"
%%% End:
