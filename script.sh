#!/bin/bash

PUBLISHED="norton tmm constant conferencia tesis erdman98:_dise norton02:_cam
chen82:_mechan summerfield14:_python phillips10:_python lutz13:_learn_python
wikipedia16:_leva fakhroutdinov15:_unified_model_languag
wikipedia16:_unified_model_languag o16:_unified o16:_unified
ltd15:_pyqt_class_refer bodnar16:_pyqt4 wiktionary16:_tappet
camnetic nortonweb serrano wwf svelocity"

for var in $PUBLISHED; do
    echo $var
    echo "\citation{$var}" >> ./main.aux
done

# echo "\citation{norton}" >> ./main.aux
# echo "\citation{tmm}" >> ./main.aux
# echo "\citation{constant}" >> ./main.aux

bibtex ./main.aux
