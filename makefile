FILES=main.tex chapters/introduccion.tex chapters/camintro.tex chapters/movement_laws.tex chapters/cam_generation.tex chapters/cam_verification.tex chapters/app_requirements.tex chapters/need_analysis.tex chapters/app_design.tex chapters/app_implementation.tex chapters/guia_usuario.tex chapters/guia_usuario.tex chapters/budget.tex chapters/environmental-impact.tex chapters/comparation.tex chapters/agradecimientos.tex

pfc_qtcam.pdf: main.pdf
	mv main.pdf pfc_qtcam.pdf

main.pdf: $(FILES)
	pdflatex main.tex
	pdflatex main.tex

.PHONY: compile compile-all clean cleanchap bibliography clean-bib

compile:
	pdflatex main.tex

compile-all: $(FILES)
	pdflatex main.tex
	./script.sh
	pdflatex main.tex
	pdflatex main.tex

bibliography:
	pdflatex main.tex
	./script.sh

clean:
	if [ -e pfc_qtcam.pdf ]; then rm ./pfc_qtcam.pdf; fi
	if [ -f main.pdf ]; then rm main.pdf; fi
	rm ./*.aux
	rm ./*.log
	rm ./*.out
	rm ./chapters/*.aux

cleanchap:
	rm ./chapters/*.aux
	rm ./chapters/*.log

clean-bib:
	rm ./main.bbl
	rm ./main.blg
	rm ./main.brf
